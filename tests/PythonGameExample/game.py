import pygame

pygame.init()
win = pygame.display.set_mode((544, 500))
pygame.display.set_caption("TestGame")

playerWalkRight = [pygame.image.load('resources/running/Right_run_1.png'),
pygame.image.load('resources/running/Right_run_2.png'),
pygame.image.load('resources/running/Right_run_4.png'),
pygame.image.load('resources/running/Right_run_5.png'),
pygame.image.load('resources/running/Right_run_6.png')]

playerWalkLeft = [pygame.image.load('resources/running/Left_run_1.png'),
pygame.image.load('resources/running/Left_run_2.png'),
pygame.image.load('resources/running/Left_run_4.png'),
pygame.image.load('resources/running/Left_run_5.png'),
pygame.image.load('resources/running/Left_run_6.png')]

playerJumpRight = [pygame.image.load('resources/Jump/Right_jump_1.png'),
pygame.image.load('resources/Jump/Right_jump_2.png')]

playerJumpRight = [pygame.image.load('resources/Jump/Left_jump_1.png'),
pygame.image.load('resources/Jump/Left_jump_2.png')]

playerStandRight = [pygame.image.load('resources/Idle/Right_stay_1.png'),
pygame.image.load('resources/Idle/Right_stay_2.png')]

playerStandLeft = [pygame.image.load('resources/Idle/Left_stay_1.png'),
pygame.image.load('resources/Idle/Left_stay_2.png')]

bg = [pygame.image.load('resources/background/parallax-mountain-trees.png'),
pygame.image.load('resources/background/parallax-mountain-mountains.png'),
pygame.image.load('resources/background/parallax-mountain-bg.png')]



x = 30
y = 350
widht = 80
height = 133
speed = 7
isJump = False
isGo = False
jumpCount = 10
left = False
right = False
animCount = 0
clock = pygame.time.Clock()

def ScreenUpdate():
    global animCount
    win.fill((255,225,255))
    win.blit(bg[2], (0, 10))
    win.blit(bg[1], (0, 330))
    win.blit(bg[0], (0, 330))

    if animCount + 1 >= 29:
        animCount = 0

    if left:
        win.blit(playerWalkLeft[animCount // 6], (x, y))
    elif right:
        win.blit(playerWalkRight[animCount // 6], (x, y))
    elif right and not(left) and not(isGo):
        win.blit(playerStandRight[0], (x, y))
    else:
        win.blit(playerStandLeft[0], (x, y))
    # pygame.draw.rect(win, (0,0,255), (x, y, widht, height))
    pygame.display.update()

run = True
while run:
    # pygame.time.delay(20)
    clock.tick(30)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and x > 5:
        x -= speed
        left = True
        right = False
        isGo = True
        animCount += 2
    elif keys[pygame.K_RIGHT] and x < 455:
        x += speed
        right = True
        left = False
        isGo = True
        animCount += 2
    else:
        animCount = 0
        left = False
        right = False
        isGo = False
    if not(isJump):
        # if keys[pygame.K_UP] and y > 5:
        #     y -= speed
        # if keys[pygame.K_DOWN] and y < 455:
        #     y +=speed
        if keys[pygame.K_UP]:
            isJump = True
    else:
        if jumpCount >= -10:
            if jumpCount < 0:
                y += (jumpCount ** 2) / 3
            else:
                y -= (jumpCount ** 2) / 3
            jumpCount -= 1
        else:
            isJump = False
            jumpCount = 10
    ScreenUpdate()

pygame.quit()