from django import forms
from .models import Post, Author


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('text',)


class CommentForms(forms.Form):

    text = forms.CharField()
    id = forms.IntegerField()


class LoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = Author
        fields = ('username', 'password', 'email')
