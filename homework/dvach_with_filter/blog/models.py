from django.db import models
import datetime
from django.contrib.auth.models import User

from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.safestring import mark_safe

from .filters import IsPostFresh
from django.contrib import admin


# Create your models here.


class Post(models.Model):
    text = models.TextField()
    published_date = models.DateTimeField(auto_now=datetime.datetime.now())
    reply_to = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True, related_name="replies")

    def __str__(self):
        return self.text


class Author(User):
    post = models.OneToOneField(Post, on_delete=models.CASCADE, null=True, blank=True)


class AuthorEditInline(admin.TabularInline):
    model = Author


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_filter = (IsPostFresh,)
    change_form_template = "duplicate_button_admin.html"
    inlines = [AuthorEditInline]
    list_display = ('text', 'replies_display',)

    def response_change(self, request, obj):
        if "_create-duplicate" in request.POST:
            posts = self.get_queryset(request)
            posts.create(text=obj.text)
            self.message_user(request, "Create duplicate")
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)

    @staticmethod
    def replies_display(obj):
        display_text = ", ".join([
            "<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, obj._meta.model_name),
                                               args=(reply.pk,)),
                                       reply.text)
            for reply in obj.replies.all()
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"
