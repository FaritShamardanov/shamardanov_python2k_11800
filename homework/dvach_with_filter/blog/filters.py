import datetime
from django.contrib import admin


# фильтр свежих постов за месяц
class IsPostFresh(admin.SimpleListFilter):
    title = 'is_post_fresh'
    parameter_name = 'is_post_fresh'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Yes'),
            ('No', 'No'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.filter(published_date__month=datetime.datetime.now().month)
        elif value == 'No':
            return queryset
        return queryset
