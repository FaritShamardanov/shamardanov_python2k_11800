from django.shortcuts import render, get_object_or_404
from blog.forms import PostForm, CommentForms, LoginForm
from blog.models import Post
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate
from django.views.generic.base import RedirectView, TemplateView, View
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.utils import timezone
from django.urls import reverse_lazy

# Create your views here.


class PostListView(ListView):
    model = Post
    paginate_by = 100
    template_name = 'post_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['now'] = timezone.now()
        return context


class PostDelete(DeleteView):
    model = Post
    template_name = 'post_confirm_delete.html'
    success_url = reverse_lazy('post-list')


class PostCreate(CreateView):
    model = Post
    fields = ['text']
    template_name = 'post_form.html'
    success_url = reverse_lazy('post-list')


class PostUpdate(UpdateView):
    model = Post
    fields = ['text']
    template_name = 'post_update_form.html'
    success_url = reverse_lazy('post-list')

def index(request):
    posts = Post.objects.all()
    form = PostForm()
    return render(request, 'post_list.html', context={'posts': posts, 'form': form})


def add_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            Post.objects.create(text=form.cleaned_data['text'])
            return HttpResponseRedirect('/')


def login(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST['email'], password=request.POST['password'])
            if user.is_active():
                return HttpResponseRedirect('/')
            else:
                return redirect('/Login')
    form = LoginForm()
    return render(request, 'login.html', context={'form': form})


def reply(request, id):
    if request.method == 'POST':
        formv = PostForm(request.POST)
        if formv.is_valid():
            Post.objects.create(text=formv.cleaned_data['text'], reply_to_id=id)
            return redirect('show_thread', id=id)
    form = CommentForms(initial={'id': id})
    return render(request, 'reply.html', context={'form': form})


def thread_index(request):
    if request.method == 'POST':
        new_thread = PostForm(request.POST)
        if new_thread.is_valid():
            Post.objects.create(text=new_thread.cleaned_data['text'])
            posts = Post.objects.all()
            form = PostForm()
            return redirect('show_thread', id=Post.objects.latest('published_date').id)
    posts = Post.objects.filter(reply_to__isnull=True).order_by('published_date')
    form = PostForm()
    return render(request, 'all_threads.html', context={'posts': posts, 'form': form})


def show_thread(request, id):
    post = Post.objects.filter(id=id)
    posts = Post.objects.filter(reply_to_id=id)
    form = PostForm()
    return render(request, 'post_list.html', context={'posts': post.union(posts).order_by('published_date'), 'form': form})