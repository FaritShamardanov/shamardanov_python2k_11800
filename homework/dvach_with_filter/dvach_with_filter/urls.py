"""dvach_with_filter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from blog.views import index, add_post, reply, thread_index, show_thread, login
from blog.views import PostListView, PostDelete, PostCreate, PostUpdate

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', index, name='index'),
    path('', PostListView.as_view(), name='post-list'),
    path('delete/<int:pk>/', PostDelete.as_view(), name='post-delete'),
    path('create/', PostCreate.as_view(), name='post-create'),
    path('update/<int:pk>/', PostUpdate.as_view(), name='post-update'),
    path('add/', add_post, name='add'),
    re_path(r'reply/(?P<id>\d+)/', reply, name='reply'),
    path('<int:id>/reply/', reply, name='add'),
    path('thread/<int:id>/', show_thread, name='show_thread'),
    path('Login/', login, name='login'),
]
