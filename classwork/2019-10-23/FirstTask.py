import argparse
import random
import os

parser = argparse.ArgumentParser()
parser.add_argument("action", type=str, help="Action")
parser.add_argument("firstNumber", type=int, help="FirstNumber")
parser.add_argument("secondArgument", type=int, help="SecondArgument")
args = parser.parse_args()

try:
    if args.action == "+" :
        print(args.firstNumber + args.secondArgument)
    elif args.action == "-" :
        print(args.firstNumber - args.secondArgument)
    elif args.action == "*":
        print(args.firstNumber * args.secondArgument)
    elif args.action == "/":
        print(args.firstNumber / args.secondArgument)
except ZeroDivisionError as e:
    print(e)
except Exception as e:
    print(e)









