import random
import argparse

days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
times = ["8:30", "10:10", "11:50", "14:00", "15:40", "17:20", "19:00", "20:40"]
parser = argparse.ArgumentParser()
parser.add_argument("--rooms", type=str, help="Rooms file")
parser.add_argument("--lessons", type=str, help="Lessons file")
parser.add_argument("--num", type=int, help="Max lessons in week")
parser.add_argument("--schedule", type=str, help="Result schedule file")
args = parser.parse_args()

num = args.num
avr_num = num // 6
residue = num % 6
num_monday = avr_num + residue

try:
    rooms_file = open(args.rooms, "r")
    rooms = rooms_file.readlines()
    rooms_file.close()
except Exception as e:
    print(e)


try:
    lessons_file = open(args.rooms, "r")
    lessons = lessons_file.readlines()
    lessons_file.close()
except Exception as e:
    print(e)

try:
    schedule_file = open(args.schedule, "w")
    for day in days:
        for time in times:
            if num <= 0:
                break
            schedule_file.write('{0}, {1}, {2}, {3}'.format(day, time, random.choice(lessons), random.choice(rooms)))
            num -= 1
    schedule_file.close()
except Exception as e:
    print(e)