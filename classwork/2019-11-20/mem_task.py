import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

text = ('Mozilla/5.0 (Windows NT 6.2)' 
        'AppleWebKit/537.36 (KHTML, like Gecko)' 
        'Chrome/28.0.1464.0 Safari/537.36')
page_link = 'https://knowyourmeme.com/memes/all'

response = requests.get(
    page_link,
    headers={'User-Agent': text}
)

print(response.content)

soup = BeautifulSoup(response.content, 'html.parser')
sours = []
images = soup.find_all('img')
pattern = 'data-src="[^"]"'
for img in images:
    if img.has_attr('data-src'):
        sours.append(str(img['data-src']))

print(sours)
count = 1
for src in sours:

    img_data = requests.get(src).content
    with open('images/' + str(count) + '.jpeg', 'wb') as handler:
        handler.write(img_data)
        count += 1






