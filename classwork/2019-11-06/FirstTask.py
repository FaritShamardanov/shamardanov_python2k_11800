import os
from datetime import datetime

path_to_folder = '/Users/faritsamardanov/Documents/shamardanov_python2k_11800/projects/starship/assets/music'


def get_delta():
    print('Введите дату начала в формате дд/мм/год')
    day_from = input()
    print('Введите дату конца в формате дд/мм/год')
    day_to = input()
    day_from = datetime.strptime(day_from, "%d/%m/%Y")
    day_to = datetime.strptime(day_to, "%d/%m/%Y")
    return day_to.timestamp(), day_from.timestamp()


def true_file_list_generator(path):
    f = {}
    time_to, time_from = get_delta()
    print(time_to, time_from)
    for file_name in os.listdir(path):
        if time_from <= os.path.getctime(path_to_folder + '/' + file_name) <= time_to \
                and str.split(path_to_folder + '/' + file_name, '.')[1] == 'mp3':
            yield path_to_folder + '/' + file_name, os.path.getctime(path_to_folder + '/' + file_name)


def write_to_m3u():
    _m3u = \
        open("/Users/faritsamardanov/Documents/shamardanov_python2k_11800/classwork/2019-11-06/testplaylist.m3u", "w")
    for song in true_file_list_generator(path_to_folder):
        _m3u.write(song[0] + "\n")
    _m3u.close()


write_to_m3u()
