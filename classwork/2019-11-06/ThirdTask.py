import datetime
from datetime import datetime



def parse_file_with_student(path):
    f = open(path, 'r')
    dates = []
    for line in f:
        line = line.strip()
        yield datetime.strptime(line, '%H:%M %d.%m.%Y %z')
    return dates


def student_counter(commit_time):
    student_count = 0
    commit_time = datetime.strptime(commit_time, '%H:%M %d.%m.%Y %z')
    for time in parse_file_with_student('students.txt'):
        if commit_time <= time:
            student_count += 1
    print(student_count)

student_counter('15:40 11.10.2019 +0000')