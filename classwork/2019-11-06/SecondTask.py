import datetime
from datetime import datetime


class OneDaySchedule(object):

    def __init__(self, day):
        self.day = day
        self.subject_tuples = [[]]

    def get_study_time_day(self):
        time = 0
        for e in self.subject_tuples:
            time += e[1]
        return time

    def get_study_time_subject(self, subject):
        time = 0
        for e in self.subject_tuples:
            if e[0] == subject:
                time += e[1]
        return time

    def add_subject(self, subject_name, time):
        self.subject_tuples.append([subject_name, time])

    def get_subjects(self):
        return self.subject_tuples[1:]


def get_time_interval(start, end):
    start = datetime.strptime(start, '%H:%M')
    end = datetime.strptime(end, '%H:%M')
    return end - start


monday = OneDaySchedule('Monday')
tuesday = OneDaySchedule('Tuesday')
wednesday = OneDaySchedule('Wednesday')
thursday = OneDaySchedule('Thursday')
friday = OneDaySchedule('Friday')
saturday = OneDaySchedule('Saturday')


def parse_txt(path):
    day = ''
    f = open(path)
    for line in f:
        line = line.strip()
        if line[3] == ':':
            day = line[0:3]
        elif day == 'Mon':
            first_split = line.split(" ")
            start_and_end = first_split[0].split("-")
            monday.add_subject(first_split[1], get_time_interval(start_and_end[0], start_and_end[1]))
        elif day == 'Tue':
            first_split = line.split(" ")
            start_and_end = first_split[0].split("-")
            tuesday.add_subject(first_split[1], get_time_interval(start_and_end[0], start_and_end[1]))
        elif day == 'Wed':
            first_split = line.split(" ")
            start_and_end = first_split[0].split("-")
            wednesday.add_subject(first_split[1], get_time_interval(start_and_end[0], start_and_end[1]))
        elif day == 'Thu':
            first_split = line.split(" ")
            start_and_end = first_split[0].split("-")
            tuesday.add_subject(first_split[1], get_time_interval(start_and_end[0], start_and_end[1]))
        elif day == 'Fri':
            first_split = line.split(" ")
            start_and_end = first_split[0].split("-")
            friday.add_subject(first_split[1], get_time_interval(start_and_end[0], start_and_end[1]))
        elif day == 'Sat':
            first_split = line.split(" ")
            start_and_end = first_split[0].split("-")
            saturday.add_subject(first_split[1], get_time_interval(start_and_end[0], start_and_end[1]))
    print(monday.subject_tuples[1:])


def sum_time_all_days(*argv):
    sum_time = datetime.strptime('00:00', '%H:%M')
    for day in argv:
        for subject in day.subject_tuples[1:]:
            sum_time += subject[1]
    return 'Часов: {0} Минут: {1}'.format(sum_time.hour, sum_time.minute)


def sum_time_at_subject(sub, *argv):
    sum_time = datetime.strptime('00:00', '%H:%M')
    for day in argv:
        for subject in day.subject_tuples[1:]:
            if subject[0] == sub:
                sum_time += subject[1]
    return 'Часов: {0} Минут: {1}'.format(sum_time.hour, sum_time.minute)


parse_txt('/Users/faritsamardanov/Documents/shamardanov_python2k_11800/classwork/2019-11-06/schedule.txt')

print(sum_time_all_days(monday, tuesday, wednesday, thursday, friday, saturday))
print(sum_time_all_days(monday))
print(sum_time_at_subject('Английский', monday, tuesday, wednesday, thursday, friday, saturday))
