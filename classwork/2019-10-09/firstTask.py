import random
from enum import Enum
import math

def generator(n):
    for n in range(n):
        a = random.randint(ord('a'), ord('z'))
        yield a


def checkInt(f):
    def wrapper(x):
        if isinstance(x, int):
            print("It's int")
            return f(x)
        else:
            print("Not int")
    return wrapper

def checkPlus(f):
    def wrapper(x):
        if (x >= 0):
            print("Bolshe nul")
            return f(x)
        else:
            print("Menshe nul")
    return wrapper

@checkInt
@checkPlus
def gen(x):
    return

print(gen(-10))
# decor(pol(0))

def getRomaNum(data):
    ones = ["","I","II","III","IV","V","VI","VII","VIII","IX"]
    tens = ["","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"]
    hunds = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"]
    thous = ["","M","MM","MMM","MMMM"]
    
    t = thous[data // 1000]
    h = hunds[data // 100 % 10]
    te = tens[data // 10 % 10]
    o =  ones[data % 10]
    
    return t+h+te+o

class Vector2d:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __add__(self, other):
        return Vector2d(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        return Vector2d(self.x - other.x, self.y - other.y)
    
    def __mul__(self, other):
        return Vector2d(self.x * other, self.y * other)
    
    def __eq__(self, other):
        return (self.x == other.x and self.y == other.y)