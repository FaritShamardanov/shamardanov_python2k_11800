import pygame
import sys

size = width, height = (300, 300)
clock = pygame.time.Clock()

pygame.init()
pygame.display.set_caption("Vk_download")
screen = pygame.display.set_mode(size)

big_rect = pygame.Rect(10, 90, 260, 30)
# rect2 = pygame.Rect(70, 90, 50, 20)
# rect3 = pygame.Rect(130, 90, 50, 20)

rect_list = [pygame.Rect(20 + x, 100, 1, 10) for x in range(240)]

how_is_draw = 0

color_grad = (255, 0, 0)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
    pygame.draw.rect(screen, (255, 255, 255), big_rect)
    keys = pygame.key.get_pressed()

    if keys[pygame.K_RIGHT]:
        color_grad = (color_grad[0] - 1, color_grad[1] + 1, 0)
        how_is_draw += 1
    elif keys[pygame.K_LEFT]:
        color_grad = (color_grad[0] + 1, color_grad[1] - 1, 0)
        how_is_draw -= 1
    for i in rect_list[0:how_is_draw]:
        pygame.draw.rect(screen, color_grad, i)
    pygame.display.flip()
    clock.tick(120)