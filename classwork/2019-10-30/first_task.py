import pygame
import sys

size = width, height = (300, 300)
clock = pygame.time.Clock()

pygame.init()
pygame.display.set_caption("Vk_download")
screen = pygame.display.set_mode(size)

rect1 = pygame.Rect(10, 90, 50, 20)
rect2 = pygame.Rect(70, 90, 50, 20)
rect3 = pygame.Rect(130, 90, 50, 20)

how_is_white = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
    if how_is_white == 0:
        pygame.draw.rect(screen, (255, 255, 255), rect1)
        pygame.draw.rect(screen, (255, 255, 0), rect2)
        pygame.draw.rect(screen, (255, 255, 0), rect3)
        how_is_white += 1
    elif how_is_white == 1:
        pygame.draw.rect(screen, (255, 255, 0), rect1)
        pygame.draw.rect(screen, (255, 255, 255), rect2)
        pygame.draw.rect(screen, (255, 255, 0), rect3)
        how_is_white += 1
    elif how_is_white == 2:
        pygame.draw.rect(screen, (255, 255, 0), rect1)
        pygame.draw.rect(screen, (255, 255, 255), rect3)
        pygame.draw.rect(screen, (255, 255, 0), rect2)
        how_is_white = 0
    pygame.display.flip()
    clock.tick(3)