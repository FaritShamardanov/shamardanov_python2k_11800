import pygame
import math

size = width, height = (900, 700)
pygame.init()
screen = pygame.display.set_mode(size)
surface = pygame.Surface(size, pygame.SRCALPHA)
clock = pygame.time.Clock()
WHITE = (255, 255, 255)
GREY = (133, 133, 133)
BLACK = pygame.Color(0, 0, 0, 60)

R = 100
angle = 0

angle_of_acceleration_and_slowing = math.pi
speed_of_acceleration = 0.5
arg_of_exp = -(angle % math.pi) * (angle % math.pi) * 0.5


def get_new_coords():
    t = math.exp(-(angle % (math.pi)) * (angle % (math.pi)) * 0.5) * 2 * math.pi
    x = R * math.cos(t)
    y = R * math.sin(t)
    return (math.floor(x + width / 2), math.floor(y + height / 2))


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

    surface.fill(BLACK)

    center = get_new_coords()
    pygame.draw.circle(screen, WHITE, center, 10)
    angle -= math.pi / 50

    screen.blit(surface, surface.get_rect())

    pygame.display.update()
    clock.tick(60)
