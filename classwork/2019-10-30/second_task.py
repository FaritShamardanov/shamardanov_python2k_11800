import pygame
import sys

size = width, height = (300, 300)
clock = pygame.time.Clock()

pygame.init()
pygame.display.set_caption("Vk_download")
screen = pygame.display.set_mode(size)

big_rect = pygame.Rect(10, 90, 260, 30)
# rect2 = pygame.Rect(70, 90, 50, 20)
# rect3 = pygame.Rect(130, 90, 50, 20)

rect_list = [pygame.Rect(20 + x * 30, 100, 20, 10) for x in range(8)]

how_is_draw = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
    pygame.draw.rect(screen, (255, 255, 255), big_rect)
    keys = pygame.key.get_pressed()

    if keys[pygame.K_RIGHT]:
        how_is_draw += 1
    elif keys[pygame.K_LEFT]:
        how_is_draw -= 1
    for i in rect_list[0:how_is_draw]:
        pygame.draw.rect(screen, (0, 0, 0), i)
    pygame.display.flip()
    clock.tick(3)