import pygame
import math

size = width, height = (900, 700)
pygame.init()
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()
WHITE = (255, 255, 255)
GREY = (133, 133, 133)
BLACK = (0, 0, 0)

angle = math.pi / 120

center = (width / 2, height / 2)
coord1 = [center, (300, 200), (600, 200)]
coord2 = [center, (300, 500), (600, 500)]


def get_v(a, b):
    x = a * math.cos(angle) + b * math.sin(angle)
    y = -a * math.sin(angle) + b * math.cos(angle)
    return (x, y)


def rotate(coords: list):
    for j, i in enumerate(coords):
        coord = get_v(i[0] - center[0], center[1] - i[1])
        coords[j] = (center[0] + coord[0], center[1] - coord[1])


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
    screen.fill(BLACK)

    pygame.draw.polygon(screen, WHITE, coord1)
    pygame.draw.polygon(screen, WHITE, coord2)

    rotate(coord1)
    rotate(coord2)

    pygame.display.update()
    clock.tick(60)
