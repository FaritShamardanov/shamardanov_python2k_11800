from django.db import models
from Market.storage_backends import PrivateMediaStorage


class Category(models.Model):
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=30)
    shop = models.ForeignKey(to='shop_and_buyer_profile_actions.Shop', on_delete=models.CASCADE,
                             related_name='products', related_query_name='products')
    quantity = models.IntegerField(default=1)
    description = models.TextField()
    is_available = models.BooleanField()
    price = models.DecimalField(max_digits=15, decimal_places=2)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='products',
                                 related_query_name='product')

    def __str__(self):
        return self.name


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images', related_query_name='image', on_delete=models.CASCADE)
    image = models.FileField(null=True, blank=True)