from abc import ABC

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import Product, Category, ProductImage
from shop_and_buyer_profile_actions.models import Shop
from django.views.generic.list import ListView
from django.urls import reverse_lazy, reverse
from django.views.generic.edit import DeleteView
from django.views.generic import CreateView, UpdateView
from .forms import ProductCreateForm
from django.views.generic.edit import FormView
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


class ProductCreateView(UserPassesTestMixin, FormView, ABC):
    form_class = ProductCreateForm
    template_name = 'shop_product_actions/product_create_form.html'
    success_url = '/'

    def test_func(self):
        if self.request.user.is_authenticated:
            return self.request.user.profile_type == 2 and self.request.user.is_authenticated
        else:
            return False

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        shop = Shop.objects.get(director_id=self.request.user.id)
        if form.is_valid():
            product = form.save(commit=False)
            product.shop = shop
            img = form.cleaned_data['image']
            product.save()
            shop.products.add(product)
            if img is not None:
                product.save()
                ProductImage.objects.create(product_id=product.id, image=img)
            shop.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ProductUpdateView(UserPassesTestMixin, UpdateView, ABC):
    model = Product
    form_class = ProductCreateForm
    template_name = 'shop_product_actions/product_update_form.html'
    success_url = '/my_products'

    def form_valid(self, form):
        ProductImage.objects.create(product_id=self.kwargs['pk'], image=form.cleaned_data['image'])
        return super().form_valid(form)

    def test_func(self):
        if self.request.user.is_authenticated:
            if self.request.user.profile_type == 2:
                shop = Shop.objects.get(pk=self.request.user.id)
                a = self.kwargs['pk']
                b = shop.products.get(pk=a) is not None
                return b


class ProductDeleteView(UserPassesTestMixin, DeleteView, ABC):
    model = Product
    success_url = reverse_lazy('shop_products_list')
    template_name = 'shop_product_actions/product_delete_form.html'

    def test_func(self):
        if self.request.user.is_authenticated:
            if self.request.user.profile_type == 2:
                shop = Shop.objects.get(pk=self.request.user.id)
                a = self.kwargs['pk']
                b = shop.products.get(pk=a) is not None
                return b


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = Category
    fields = ('name',)
    template_name = 'shop_product_actions/category_create_form.html'
    success_url = '/'


class ShopProductListView(LoginRequiredMixin, ListView):
    model = Product
    paginate_by = 10
    template_name = 'shop_product_actions/shop_products_list.html'

    def get_queryset(self):
        return Product.objects.filter(shop__director__email=self.request.user.email)


# @method_decorator(login_required(), name='dispathch')
# class ProductDetailView(DetailView):
#     model =Product


class ProductList(UserPassesTestMixin, ListView, ABC):
    model = Product
    # paginate_by = 10
    template_name = 'shop_product_actions/product_list.html'

    def get_queryset(self):
        availability = Q(is_available=True)
        quantity_more_that_one = Q(quantity__gte=2)
        return Product.objects.filter(availability & quantity_more_that_one)

    def test_func(self):
        if self.request.user.is_authenticated:
            return self.request.user.profile_type == 1
        return False

    def get_login_url(self):
        return reverse('login')





