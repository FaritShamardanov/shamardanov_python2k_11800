from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Product, Category, ProductImage

# Register your models here.

admin.site.register(Product)
admin.site.register(Category)
admin.site.register(ProductImage)


class ProductImageInline(admin.StackedInline):
    model = ProductImage


class ProductAdmin(admin.ModelAdmin):
    fields = ('name', 'shop', 'quantity', 'is_available', 'price', 'category')
    inlines = [ProductImageInline]

    readonly_fields = ["product_image", ]

    def product_image(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.image.url,
            width=obj.image.width,
            height=obj.image.height,
        )
    )
