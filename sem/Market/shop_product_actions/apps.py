from django.apps import AppConfig


class ShopProductActionsConfig(AppConfig):
    name = 'shop_product_actions'
