from django.urls import path, include
from shop_product_actions.views import ProductCreateView, CategoryCreateView, ShopProductListView,\
    ProductUpdateView, ProductDeleteView, ProductList

urlpatterns = [
    path('<pk>/product_create', ProductCreateView.as_view(), name='product_create'),
    path('<pk>/category_create', CategoryCreateView.as_view(), name='category_create'),
    path('my_products', ShopProductListView.as_view(), name='shop_products_list'),
    path('<pk>/product_update', ProductUpdateView.as_view(), name='product_update'),
    path('<pk>/product_delete', ProductDeleteView.as_view(), name='product_delete'),
    path('all_products', ProductList.as_view(), name='product_list'),
]