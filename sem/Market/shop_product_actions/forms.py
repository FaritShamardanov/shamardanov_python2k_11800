from django import forms
from .models import Category, Product


class ProductCreateForm(forms.ModelForm):
    image = forms.ImageField(required=False)

    class Meta:
        model = Product
        fields = ['name', 'description', 'quantity', 'is_available', 'price', 'category', ]