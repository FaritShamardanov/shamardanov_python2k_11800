from django.apps import AppConfig


class ShopAndBuyerProfileActionsConfig(AppConfig):
    name = 'shop_and_buyer_profile_actions'
