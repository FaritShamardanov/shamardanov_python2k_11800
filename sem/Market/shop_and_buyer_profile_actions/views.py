from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import UpdateView, CreateView
# from .forms import ProfileUpdateForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from .models import Buyer, Profile, Shop, Address
from rest_framework import viewsets
from .serializers import BuyerSerializer, ShopSerializer


# Create your views here.
@method_decorator(login_required(), name='dispatch')
class ProfileUpdateView(UpdateView):
    model = Profile
    fields = ('avatar', 'first_name', 'last_name', 'email',)
    template_name = 'shop_and_buyer_profile_actions/buyer_update_form.html'
    success_url = '/'

    def get_queryset(self):
        query_set = Profile.objects.filter(email=self.request.user.email)
        return query_set


@method_decorator(login_required(), name='dispatch')
class ShopUpdateView(UpdateView):
    model = Shop
    fields = ('shop_name',)
    template_name = 'shop_and_buyer_profile_actions/shop_update_form.html'
    success_url = '/'

    def get_queryset(self):
        query_set = Shop.objects.filter(director__email=self.request.user.email)
        return query_set


class BuyerCreateView(LoginRequiredMixin, CreateView):
    model = Buyer


class ShopCreateView(LoginRequiredMixin, CreateView):
    model = Shop
    fields = ('shop_name',)
    template_name = 'shop_and_buyer_profile_actions/shop_update_form.html'
    success_url = '/'

    # def post(self, request, *args, **kwargs):
    #     form = self.get_form()
    #     if form.is_valid():
    #         Shop.objects.create(director_id=request.user.id, shop_name=form.cleaned_data['shop_name'])
    #         return self.form_valid(form)
    #     else:
    #         return self.form_invalid(form)


@method_decorator(login_required(), name='dispatch')
class AddressUpdateView(UpdateView):
    model = Address
    fields = ('country', 'region', 'city', 'street', 'building', 'floor', 'flat', 'index',)
    template_name = 'shop_and_buyer_profile_actions/address_update_form.html'
    success_url = '/'

    def get_queryset(self):
        query_set = Address.objects.filter(profile__email=self.request.user.email)
        return query_set


@method_decorator(login_required(), name='dispatch')
class AddressCreateView(CreateView):
    model = Address
    template_name = 'shop_and_buyer_profile_actions/address_create_form.html'
    fields = ('country', 'region', 'city', 'street', 'building', 'floor', 'flat', 'index',)
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            address = form.save()
            Profile.objects.filter(email=self.request.user.email).update(address=address)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class BuyerDetailView(viewsets.ModelViewSet):
    queryset = Buyer.objects.all()
    serializer_class = BuyerSerializer


class ShopDetailView(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer







