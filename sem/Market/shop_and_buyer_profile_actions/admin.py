from django.contrib import admin
from django.utils.safestring import mark_safe
from django.urls import reverse

from . import models
from shop_and_buyer_profile_actions.models import Profile, Buyer, Shop
from buyer_basket.admin import BasketAdmin
from django.contrib.auth.admin import UserAdmin

# Register your models here.

admin.site.register(models.Buyer)
admin.site.register(models.Shop)


class ShopInline(admin.StackedInline):
    model = Shop
    show_change_link = True


class BuyerInline(admin.StackedInline):
    model = Buyer
    show_change_link = True


@admin.register(Profile)
class ProfileAdmin(UserAdmin):
    inlines = [BuyerInline, ShopInline]
    list_display = ('email', 'first_name', 'last_name', 'date_joined', 'last_login', 'is_admin', 'is_staff',)
    search_fields = ('email', 'first_name', 'last_name',)
    readonly_fields = ('date_joined', 'last_login',)

    filter_horizontal = ()
    list_filter = ('date_joined', 'is_admin', 'is_staff', 'last_login')
    fieldsets = ()
    ordering = ()



