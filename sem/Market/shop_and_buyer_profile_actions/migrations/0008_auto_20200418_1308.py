# Generated by Django 3.0.5 on 2020-04-18 13:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_and_buyer_profile_actions', '0007_auto_20200411_0116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
