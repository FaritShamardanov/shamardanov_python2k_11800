# Generated by Django 3.0.5 on 2020-04-19 18:35

import Market.storage_backends
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop_and_buyer_profile_actions', '0012_auto_20200419_1645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='avatar',
            field=models.FileField(blank=True, null=True, storage=Market.storage_backends.PrivateMediaStorage(), upload_to=''),
        ),
    ]
