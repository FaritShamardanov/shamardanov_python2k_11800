from django.urls import path, include
from .views import ProfileUpdateView, ShopUpdateView, AddressUpdateView, AddressCreateView, ShopCreateView, \
    BuyerDetailView, ShopDetailView

from rest_framework import routers

router = routers.DefaultRouter()
router.register('buyer_detail', BuyerDetailView)
router.register('shop_detail', ShopDetailView)

urlpatterns = [
    path('<pk>/profile_update', ProfileUpdateView.as_view(), name='profile_update'),
    path('<pk>/shop_update', ShopUpdateView.as_view(), name='shop_update'),
    path('<pk>/shop_create', ShopCreateView.as_view(), name='shop_create'),
    path('<pk>/address_update', AddressUpdateView.as_view(), name='address_update'),
    path('<pk>/address_create', AddressCreateView.as_view(), name='address_create'),
    path('api_buyer/', include(router.urls)),
]