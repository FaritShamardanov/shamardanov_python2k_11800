from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save

from Market.storage_backends import PrivateMediaStorage
from buyer_basket.models import Basket
from home.models import Address
from django.dispatch import receiver


# class Buyer(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE, null=False)
#     delivery_address = models.OneToOneField(Address, on_delete=models.CASCADE)
#     basket = models.OneToOneField(Basket, on_delete=models.CASCADE, null=True, blank=True)
#     account_create_date = models.DateField(auto_now_add=True)


class ProfileManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, profile_type, password=None):
        if not email:
            raise ValueError("Profile must have an email address")
        if not first_name:
            raise ValueError("Profile must have a first name")
        if not last_name:
            raise ValueError("Profile must have a last name")
        if not profile_type:
            raise ValueError("Profile must have a profile type")

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            profile_type=profile_type,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, profile_type, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            first_name=first_name,
            last_name=last_name,
            profile_type=profile_type,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Profile(AbstractBaseUser):
    email = models.EmailField(verbose_name='email', max_length=60, unique=True)
    first_name = models.CharField(max_length=30, unique=False)
    last_name = models.CharField(max_length=30, unique=False)
    avatar = models.FileField(null=True, blank=True, storage=PrivateMediaStorage())
    address = models.OneToOneField(Address, on_delete=models.CASCADE, null=True, blank=True, related_name='profile')
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    PROFILE_TYPE_CHOICES = (
        (1, 'buyer'),
        (2, 'shop'),
    )
    profile_type = models.PositiveSmallIntegerField(choices=PROFILE_TYPE_CHOICES, verbose_name='profile type',
                                                    null=False, blank=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'profile_type', 'avatar',]
    EMAIL_FIELD = 'email'

    objects = ProfileManager()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


@receiver(post_save, sender=Profile)
def create_buyer(sender, instance, created, raw, **kwargs):
    if created:
        Buyer.objects.create(person=instance)
        # print('Buyer is created!')


@receiver(post_save, sender=Profile)
def create_shop(sender, instance, created, raw, **kwargs):
    if created:
        Shop.objects.create(director=instance)
        # print('Shop is created!')


class Buyer(models.Model):
    person = models.OneToOneField(Profile, on_delete=models.CASCADE, primary_key=True)
    # basket = models.OneToOneField(Basket, on_delete=models.SET_NULL, null=True, blank=True, related_name='buyer')

    def __str__(self):
        return self.person.first_name + " " + self.person.last_name


class Shop(models.Model):
    director = models.OneToOneField(Profile, on_delete=models.CASCADE, primary_key=True, related_name='shop')
    shop_name = models.CharField(max_length=50)

    def __str__(self):
        return self.shop_name
