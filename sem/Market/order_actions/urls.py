from django.urls import path, re_path, include
from  .views import create_order, OrderList, OrderDetailView, OrderView, StatusUpdateView, ShipmentUpdateView
from rest_framework import routers

router = routers.DefaultRouter()
router.register('orders', OrderView, basename='Order')

urlpatterns = [
    path('create_order/', create_order, name='create_order'),
    path('<pk>/order_list', OrderList.as_view(), name='order_list'),
    path('<pk>/order_detail', OrderDetailView.as_view(), name='order_detail'),
    path('<pk>/status_update', StatusUpdateView.as_view(), name='status_update'),
    path('<pk>/shipment_update', ShipmentUpdateView.as_view(), name='shipment_update'),
    path('api_orders/', include(router.urls)),
]