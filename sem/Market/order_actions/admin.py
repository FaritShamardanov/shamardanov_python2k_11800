from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import Order, ProductInOrder, Status, Shipment

# Register your models here.


# admin.site.register(Order)
# admin.site.register(ProductInOrder)
admin.site.register(Status)
admin.site.register(Shipment)


class ShipmentInline(admin.StackedInline):
    model = Shipment


@admin.register(ProductInOrder)
class ProductInOrderAdmin(admin.ModelAdmin):
    list_filter = ('order', 'product')
    inlines = [ShipmentInline]


class ProductInOrderInline(admin.StackedInline):
    model = ProductInOrder


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('buyer', 'create_date')
    list_display_links = ('buyer',)
    list_filter = ('create_date',)
    inlines = [ProductInOrderInline]
