from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from shop_and_buyer_profile_actions.models import Buyer, Shop
from shop_product_actions.models import Product


class Order(models.Model):
    create_date = models.DateField(auto_now_add=True)
    buyer = models.ForeignKey(Buyer, related_name='orders', related_query_name='order', on_delete=models.CASCADE)
    shops = models.ManyToManyField(Shop, related_name='orders', related_query_name='order')

    def get_order_price(self):
        order_price = 0
        products_in_order = self.products_in_order
        for product in products_in_order:
            order_price += product.price * product.quantity
        return order_price


class ProductInOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='products_in_order',
                              related_query_name='product_in_order')
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.IntegerField()

    def __str__(self):
        return self.product.name


@receiver(post_save, sender=ProductInOrder)
def change_product_quantity(sender, instance, created, **kwargs):
    if created:
        product = Product.objects.get(id=instance.product.id)
        product.quantity -= instance.quantity
        product.save()


class Shipment(models.Model):
    product = models.OneToOneField(ProductInOrder, on_delete=models.CASCADE, related_name='shipment', null=False, blank=False)
    shipment_company_name = models.CharField(max_length=40, null=False, blank=False, default='Pickup')
    shipment_cost = models.DecimalField(max_digits=15, decimal_places=2, default=0)


class Status(models.Model):
    ACCEPTED = 'AC'
    CONFIRMED = 'CD'
    READY_TO_SENT = 'RS'
    SENT = 'ST'
    READY_TO_RECEIVE = 'RR'
    RECEIVED = 'RD'
    STATUS_CHOICES = [
        (ACCEPTED, 'Accepted'),
        (CONFIRMED, 'Confirmed'),
        (READY_TO_SENT, 'Ready to sent'),
        (SENT, 'Sent'),
        (READY_TO_RECEIVE, 'Ready to receive'),
        (RECEIVED, 'Received'),
    ]
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=ACCEPTED)
    shipment = models.OneToOneField(Shipment, on_delete=models.CASCADE, related_name='status')
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.status


@receiver(post_save, sender=ProductInOrder)
def create_default_shipment(sender, instance, created, **kwargs):
    if created:
        Shipment.objects.create(product=instance)


@receiver(post_save, sender=Shipment)
def create_default_status(sender, instance, created, **kwargs):
    if created:
        Status.objects.create(shipment=instance)
