from django.apps import AppConfig


class BuyerOrderConfig(AppConfig):
    name = 'order_actions'
