# Generated by Django 3.0.5 on 2020-04-15 19:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order_actions', '0004_auto_20200411_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipment',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='shipment', to='order_actions.ProductInOrder'),
        ),
        migrations.AlterField(
            model_name='status',
            name='shipment',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='status', to='order_actions.Shipment'),
        ),
    ]
