# Generated by Django 3.0.5 on 2020-04-11 15:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order_actions', '0003_auto_20200411_1104'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shipment',
            name='order',
        ),
        migrations.AddField(
            model_name='shipment',
            name='product',
            field=models.OneToOneField(default='', on_delete=django.db.models.deletion.CASCADE, related_name='shipments', to='order_actions.ProductInOrder'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shipment',
            name='shipment_cost',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=15),
        ),
    ]
