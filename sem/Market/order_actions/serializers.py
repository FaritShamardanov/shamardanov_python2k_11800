from rest_framework import serializers
from .models import Order, ProductInOrder


class ProductInOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductInOrder
        fields = '__all__'


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    products_in_order = ProductInOrderSerializer(many=True)

    class Meta:
        model = Order
        fields = ('id', 'create_date', 'buyer', 'shops', 'products_in_order')
