from abc import ABC
from django.http import Http404

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Q
from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, UpdateView

from .tasks import send_success_order_email
from rest_framework import viewsets, permissions
from .serializers import OrderSerializer

from .models import Buyer, Product, Order, Shop, ProductInOrder, Status, Shipment
from home.models import Address
from buyer_basket.models import Basket, ProductInBasket
from registration_and_authorization.permissions import UserPermissions

# Create your views here.


def buyer_check(user):
    return user.profile_type == 1


@login_required(login_url='login/')
@user_passes_test(buyer_check)
def create_order(request):
    if request.method == "GET" and request.user.address is not None:
        address = Address.objects.get(profile__id=request.user.id)
        basket = Basket.objects.get(buyer__person_id=request.user.id)
        prb = ProductInBasket.objects.filter(basket=basket)
        if basket is not None and address is not None:
            products = Product.objects.filter(product_in_basket__basket=basket)
            if products.count() >= 1:
                return render(request, 'order_actions/order_page.html', {'address': address, 'products': products})
        return HttpResponseNotFound("Basket is empty or address is empty")
    elif request.method == "POST":
        buyer = Buyer.objects.get(person=request.user)
        basket = Basket.objects.get(buyer__person=request.user)
        products = Product.objects.filter(product_in_basket__basket=basket)
        if buyer.person.address is not None:
            shops = Shop.objects.filter(products__in=products)
            order = Order.objects.create(buyer=buyer)
            order.shops.set(shops)
            products = basket.products_in_basket.all()
            for product in products:
                ProductInOrder.objects.create(product=product.product, quantity=product.quantity, order=order)
            Basket.objects.filter(buyer__person=request.user).delete()
            send_success_order_email.delay(buyer.person.email, 'Успешный заказ в Market!')
            return redirect('/')
    raise Http404('Адрес не указан')


class OrderList(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'order_actions/order_list.html'

    def get_queryset(self):
            # b_order = Buyer.objects.filter(person_id=self.request.user.id)
            # s_order = Shop.objects.filter(director_id=self.request.user.id)
        if self.request.user.profile_type == 1:
            return Order.objects.filter(buyer__person_id=self.request.user.id)
        elif self.request.user.profile_type == 2:
            res = Order.objects.filter(shops__director=self.request.user)
            return res


class OrderDetailView(DetailView):
    model = Order
    template_name = 'order_actions/order_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.profile_type == 1:
            context['products'] = ProductInOrder.objects.filter(order_id=self.kwargs['pk'])
            return context
        elif self.request.user.profile_type == 2:
            context['products'] = ProductInOrder.objects\
                .filter(order_id=self.kwargs['pk'], product__shop_id=self.request.user.id)
            return context


class StatusUpdateView(UserPassesTestMixin, UpdateView):
    model = Status
    template_name = 'order_actions/status_update_view.html'
    fields = ('status',)
    success_url = 'order_list'

    def test_func(self):
        if self.request.user.is_authenticated:
            if self.request.user.profile_type == 2:
                orders = Order.objects.filter(shops__director_id=self.request.user.id)
                a = self.kwargs['pk']
                return Shipment.objects.filter(product__order__in=orders) is not None


class ShipmentUpdateView(UserPassesTestMixin, UpdateView, ABC):
    model = Shipment
    template_name = 'order_actions/shipment_update_view.html'
    fields = ('shipment_company_name', 'shipment_cost', )
    success_url = 'order_list'

    def test_func(self):
        if self.request.user.is_authenticated:
            if self.request.user.profile_type == 2:
                orders = Order.objects.filter(shops__director_id=self.request.user.id)
                a = self.kwargs['pk']
                return Shipment.objects.filter(product__order__in=orders) is not None


class OrderView(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (UserPermissions,)

    def get_queryset(self):
        orders = Order.objects.filter(shops__director_id=self.request.user.id)
        for order in orders:
            order.shops.set(order.shops.filter(director_id=self.request.user.id))
            order.products_in_order.set(order.products_in_order.filter(order__shops__director_id=self.request.user.id))
        return orders




        

