from celery import shared_task

from shop_and_buyer_profile_actions.models import Profile
from django.contrib.auth.forms import PasswordResetForm


@shared_task
def reset_password_with_email(subject_template_name, email_template_name, context,
                              from_email, to_email, html_email_template_name):
    context['user'] = Profile.objects.get(email=context['user'])

    PasswordResetForm.send_mail(
        None,
        subject_template_name,
        email_template_name,
        context,
        from_email,
        to_email,
        html_email_template_name
    )
