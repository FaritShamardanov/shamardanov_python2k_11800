from django.apps import AppConfig


class RegistrationAndAuthorizationConfig(AppConfig):
    name = 'registration_and_authorization'
