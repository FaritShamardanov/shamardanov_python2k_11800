from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout, views
from registration_and_authorization.forms import RegistrationForm, AuthenticationForm, PasswordResetForm
from shop_and_buyer_profile_actions.models import Buyer, Profile, Shop
from .forms import PasswordResetForm as ResetForm


def registration_view(request):
    context = {}
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(email=email, password=raw_password)
            login(request, account)
            return redirect('home')
        else:
            context['registration_form'] = form
    else:
        form = RegistrationForm()
        context['registration_form'] = form
    return render(request, 'registration_and_authorization/registration.html', context)


def logout_view(request):
    logout(request)
    return redirect('home')


def login_view(request):
    context ={}
    user = request.user

    if user.is_authenticated:
        return redirect('home')
    if request.POST:
        form = AuthenticationForm(request.POST)
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(email=email, password=password)
            if user:
                login(request, user)
                return redirect('home')
    else:
        form = AuthenticationForm()
    context['login_form'] = form
    return render(request, 'registration_and_authorization/login.html', context)


class PasswordResetView(views.PasswordResetView):
    form_class = ResetForm


