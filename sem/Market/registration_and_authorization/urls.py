from django.urls import path, include
from django.contrib.auth import views
from .views import registration_view, logout_view, login_view
from .views import PasswordResetView as Reset
from celery import shared_task

urlpatterns = [
    path('register/', registration_view, name='registration'),
    path('logout/', logout_view, name='logout'),
    path('login/', login_view, name='login'),
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('api-auth/', include('rest_framework.urls')),
]