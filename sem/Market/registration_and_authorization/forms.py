from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from registration_and_authorization.tasks import reset_password_with_email
from django.contrib.auth.forms import PasswordResetForm as PasswordResetFormCore

from shop_and_buyer_profile_actions.models import Profile


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(max_length=60, help_text='Required. ADd a valid email address')

    class Meta:
        model = Profile
        fields = ('email', 'first_name', 'last_name', 'profile_type', 'password1', 'password2')


class AuthenticationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model = Profile
        fields = ('email', 'password',)

    def clean(self):
        if self.is_valid():
            email = self.cleaned_data['email']
            password = self.cleaned_data['password']
            if not authenticate(email=email, password=password):
                raise forms.ValidationError('Invalid login')


class PasswordResetForm(PasswordResetFormCore):
    email = forms.EmailField(max_length=254, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'id': 'email',
            'placeholder': 'Email'
        }
    ))

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        context['user'] = context['user'].id

        reset_password_with_email.delay(subject_template_name=subject_template_name,
                                        email_template_name=email_template_name,
                                        context=context, from_email=from_email, to_email=to_email,
                                        html_email_template_name=html_email_template_name)
