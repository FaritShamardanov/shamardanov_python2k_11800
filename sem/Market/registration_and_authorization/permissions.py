from rest_framework import permissions
from shop_and_buyer_profile_actions.models import Profile


class UserPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            profile = Profile.objects.get(id=request.user.id)
            return profile.profile_type == 2
