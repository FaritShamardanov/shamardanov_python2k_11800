from django.urls import path, re_path
from .views import add_to_basket, BasketList, BasketProductDeleteView

urlpatterns = [
    re_path(r'add_to_basket/(?P<product_id>[0-9]+)/to/(?P<user_id>[0-9]+)', add_to_basket),
    path('<pk>/basket', BasketList.as_view(), name='basket'),
    path('<pk>/delete_from_basket', BasketProductDeleteView.as_view(), name='delete_product_from_basket'),
]