from __future__ import absolute_import, unicode_literals
from celery import task
from .models import Basket
from datetime import datetime, timedelta


@task()
def clean_old_baskets():
    Basket.objects.filter(last_change__lte=(datetime.now() - timedelta(days=7))).delete()