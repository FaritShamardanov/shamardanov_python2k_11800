from django.contrib import admin

# Register your models here.
from buyer_basket.models import Basket, ProductInBasket


class ProductsInBasketTabularInline(admin.TabularInline):
    model = ProductInBasket


@admin.register(Basket)
class BasketAdmin(admin.ModelAdmin):

    list_display = ('basket_buyer', 'create_date', 'last_change')
    list_filter = ('create_date', 'last_change')
    inlines = [ProductsInBasketTabularInline]

