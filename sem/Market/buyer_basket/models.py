from django.db import models
from shop_product_actions.models import Product


class Basket(models.Model):
    # total_price = models.DecimalField(max_digits=6, decimal_places=2)
    buyer = models.OneToOneField(to='shop_and_buyer_profile_actions.Buyer', on_delete=models.CASCADE, null=True)
    create_date = models.DateField(auto_now_add=True)
    last_change = models.DateField(auto_now=True)

    def get_basket_total_price(self):
        basket_total_price = 0
        products = self.products_in_basket.all()
        for product in products:
            basket_total_price += product.quantity * product.product.price
        return basket_total_price

    def basket_buyer(self):
        return self.buyer.person.first_name + " " + self.buyer.person.last_name


class ProductInBasket(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE, related_name='products_in_basket',
                               related_query_name='product_in_basket')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, unique=False, related_name='products_in_basket',
                                related_query_name='product_in_basket')
    quantity = models.IntegerField(default=1)

    def get_total_price(self):
        return self.quantity * self.product.price

    def __str__(self):
        return self.product.name
