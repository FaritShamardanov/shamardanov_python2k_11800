from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.views.generic import ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Basket, ProductInBasket, Product
from shop_and_buyer_profile_actions.models import Buyer
from django.contrib.auth.decorators import user_passes_test, login_required
from django.shortcuts import get_object_or_404

# Create your views here.


def buyer_check(user):
    return user.profile_type == 1


@login_required(login_url='login/')
@user_passes_test(buyer_check)
def add_to_basket(request, product_id, user_id):
    if request.method == "POST":
        count = request.POST['count']
        q = Product.objects.get(id=product_id).quantity
        if int(count) > q:
            raise ValidationError(
                _('Invalid value: %(value)s'),
                code='invalid',
                params={'value': count},
            )
        buyer = Buyer.objects.get(person_id=request.user)
        basket, basket_created = Basket.objects.get_or_create(buyer=buyer)
        product, product_created = ProductInBasket.objects.get_or_create(product_id=product_id, basket_id=basket.id)
        if product_created is False:
            product.quantity += int(count)
            product.save()
        else:
            product.quantity = count
            product.save()
        if basket_created is True:
            buyer.basket = basket
        buyer.save()
        return redirect('/')


class BasketList(LoginRequiredMixin, ListView):
    model = Basket
    template_name = 'buyer_basket/basket_list.html'
    login_url = 'login/'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        if Basket.objects.filter(buyer__person_id=self.request.user.id).exists():
            context['basket_total_price'] = Basket.objects.get(buyer__person_id=self.request.user.id)\
                .get_basket_total_price
        return context

    def get_queryset(self):
        if Basket.objects.filter(buyer__person_id=self.request.user.id).exists():
            return ProductInBasket.objects.filter(basket__buyer__person_id=self.request.user.id)
        # return None


class BasketProductDeleteView(LoginRequiredMixin, DeleteView):
    model = ProductInBasket
    template_name = 'buyer_basket/product_delete_form.html'
    login_url = 'login/'
    success_url = '/'


