from django.apps import AppConfig


class BuyerBusketConfig(AppConfig):
    name = 'buyer_basket'
