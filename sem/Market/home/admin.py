from django.contrib import admin
from .models import Address

# Register your models here.

# admin.site.register(Address)


class AddressTabularInline(admin.TabularInline):
    model = Address


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('get_user', '__str__')
