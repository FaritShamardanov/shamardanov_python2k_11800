# Generated by Django 3.0.5 on 2020-04-05 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(max_length=30)),
                ('region', models.CharField(max_length=30)),
                ('city', models.CharField(max_length=30)),
                ('street', models.CharField(max_length=50)),
                ('building', models.IntegerField()),
                ('floor', models.IntegerField(blank=True, null=True)),
                ('flat', models.IntegerField(blank=True, null=True)),
                ('index', models.IntegerField()),
            ],
        ),
    ]
