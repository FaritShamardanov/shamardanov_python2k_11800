from django.db import models


class Address(models.Model):
    country = models.CharField(max_length=30)
    region = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    street = models.CharField(max_length=50)
    building = models.IntegerField()
    floor = models.IntegerField(null=True, blank=True)
    flat = models.IntegerField(null=True, blank=True)
    index = models.IntegerField()

    def __str__(self):
        delimiter = ', '
        return delimiter.join((self.country, self.region, self.city))

    def get_user(self):
        return self.profile.first_name + " " + self.profile.last_name
