from django.urls import path
from . import views

app_name = 'articles'
urlpatterns = [
    path('', views.index, name='index'),
    path('article/<int:article_id>/', views.detail, name='detail'),
    path('article/<int:article_id>/add_comment/', views.add_comment, name='add_comment'),
]