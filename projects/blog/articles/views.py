from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Article, Comment
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from .forms import ArticleForm
from django.utils import timezone

# Create your views here.


def index(request):
    latest_articles_list = Article.objects.order_by('-published_date')
    form = ArticleForm()
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            Article.objects.create(article_text=form.cleaned_data['article_text'],
                                   article_title=form.cleaned_data['article_title'],
                                   published_date=timezone.now())
            # return render(request, 'articles/list.html', {'latest_articles_list': latest_articles_list, 'form': form})
            return redirect('articles:index')
    return render(request, 'articles/list.html', {'latest_articles_list': latest_articles_list, 'form': form})


def detail(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    articles = Article.objects.filter(id=article_id)
    return render(request, 'articles/detail.html', {'article': article, 'articles': articles})


def add_comment(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    article.comment_set.create(author_name=request.POST['name'], comment_text=request.POST['text'])
    return render(request, 'articles/detail.html', {'article': article})
