from django.db import models
from django.utils import timezone
import datetime

# Create your models here.


class Article(models.Model):
    article_title = models.CharField('Article title', max_length=100)
    article_text = models.TextField('Article text')
    published_date = models.DateTimeField('Article published date')

    def __str__(self):
        return self.article_title

    def was_published_recently(self):
        return self.published_date >= (timezone.now() - datetime.timedelta(days=7))


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField('Author name', max_length=50)
    comment_text = models.CharField('Comment text', max_length=200)

    def __str__(self):
        return self.author_name
