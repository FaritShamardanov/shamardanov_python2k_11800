import pygame, sys
from game_objects import *
from game_settings import restart


class Menu:

    def __init__(self, buttons=[120, 140, u'Button', (250, 250, 30), (250, 30, 250), 0]):
        self.buttons = buttons

    def render(self, surf, font, num_punkt):
        for i in self.buttons:
            if num_punkt == i[5]:
                surf.blit(font.render(i[2], 1, i[4]), (i[0], i[1]))
            else:
                surf.blit(font.render(i[2], 1, i[3]), (i[0], i[1]))

    def menu(self, win, screen):
        done = True
        font_menu = pygame.font.Font(None, 50)
        pygame.key.set_repeat(0, 0)
        pygame.mouse.set_visible(True)
        button = 0
        bg = pygame.image.load('assets/space.png')
        while done:
            # screen.fill((0, 0, 0))
            screen.blit(bg, (0, 0))

            mp = pygame.mouse.get_pos()
            for i in self.buttons:
                if mp[0] > i[0] and mp[0] < i[0] + 155 and mp[1] > i[1] and mp[1] < i[1] + 50:
                    button = i[5]
            self.render(screen, font_menu, button)
            keys = pygame.key.get_pressed()
            if keys[pygame.K_q]:
                return True

            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    sys.exit()
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        sys.exit()
                    '''блок навигации меню с помощью клавиш'''
                    if e.key == pygame.K_UP:
                        if button > 0:
                            button -= 1
                    if e.key == pygame.K_DOWN:
                        if button < len(self.buttons) - 1:
                            button += 1
                    '''//'''
                if e.type == pygame.MOUSEBUTTONDOWN and e.button == 1:
                    if button == 0:
                        done = False
                    elif button == 1:
                        sys.exit()
                    # elif button == 3:
                    # return True

            win.blit(screen, (0, 0))
            pygame.display.flip()
