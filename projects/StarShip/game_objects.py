import pygame
import random
from game_settings import HEIGHT, WIDTH
from menu import *
from mbuttons import *
#from main import screen, window

#main player class
class Player(pygame.sprite.Sprite):
    speed = 20
    shooting_cooldown = 150

    def __init__(self, clock, bullets):
        super(Player, self).__init__()

        self.clock = clock
        self.bullets = bullets
        self.image = pygame.image.load('assets/ship.png')
        self.rect = self.image.get_rect()
        self.rect.bottom = HEIGHT - 10
        self.current_speed_x = 0
        self.current_speed_y = 0
        self.health = 2
        self.current_shooting_cooldown = 0
        self.bullet_sound = pygame.mixer.Sound('assets/music/bullet_sound.wav')

#player position update method
    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and self.rect.x >= 10:
            self.current_speed_x = - self.speed
        elif keys[pygame.K_RIGHT] and self.rect.x <= WIDTH - 105:
            self.current_speed_x = self.speed
        elif keys[pygame.K_UP] and self.rect.y >= (HEIGHT / 2):
            self.current_speed_y = -self.speed
        elif keys[pygame.K_DOWN] and self.rect.y <= HEIGHT - 105:
            self.current_speed_y = self.speed
        else:
            self.current_speed_x = 0
            self.current_speed_y = 0
        self.rect.move_ip((self.current_speed_x, self.current_speed_y))
        self.process_shooting()
#method player shooting
    def process_shooting(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_SPACE] and self.current_shooting_cooldown <= 0:
            self.bullets.add(Bullet((self.rect.midtop[0], self.rect.midtop[1] - 15), -25))
            self.current_shooting_cooldown = self.shooting_cooldown
            self.bullet_sound.play()
        else:
            self.current_shooting_cooldown -= self.clock.get_time()

        for bullet in list(self.bullets):
            if bullet.rect.bottom < 0:
                self.bullets.remove(bullet)

#background game class
class Background(pygame.sprite.Sprite):
    def __init__(self):
        super(Background, self).__init__()
        self.image = pygame.image.load('assets/space.png')
        self.rect = self.image.get_rect()
        self.rect.bottom = HEIGHT

    def update(self):
        self.rect.bottom += 1
        if self.rect.bottom >= self.rect.height:
            self.rect.bottom = HEIGHT

#metheor class
class Meteor(pygame.sprite.Sprite):
    cooldown = 250
    current_cooldown = 0
    speed = 10

    def __init__(self):
        super(Meteor, self).__init__()
        image_name = 'assets/meteors/meteor_{}.png'.format(random.randint(1, 27))
        self.image = pygame.image.load(image_name)
        self.rect = self.image.get_rect()
        self.rect.midbottom = (random.randint(0, WIDTH), 0)
#metheor position update class
    def update(self):
        self.rect.move_ip((0, self.speed))
#metheor generating class
    @staticmethod
    def process_meteors(clock, meteorites, tiks):
        if Meteor.current_cooldown <= 0:
            meteorites.add(Meteor())
            Meteor.current_cooldown = Meteor.cooldown
        else:
            Meteor.current_cooldown -= 0.5 * clock.get_time()

        if tiks % 17 == 0:
            Meteor.speed += 0.6

        for m in list(meteorites):
            if (m.rect.right < 0 or
                    m.rect.left > WIDTH or
                    m.rect.top > HEIGHT):
                meteorites.remove(m)

#boss class
class BadGuy(pygame.sprite.Sprite):
    speed = 10
    shooting_cooldown = 150

    def __init__(self, clock, bullets):
        super(BadGuy, self).__init__()
        self.clock = clock
        self.bullets = bullets
        self.image = pygame.image.load('assets/bad_guy.png')
        self.rect = self.image.get_rect()
        self.current_shooting_cooldown = 0
        self.rect.midbottom = (random.randint(0, WIDTH), 150)
        self.bad_guy_sound = pygame.mixer.Sound('assets/music/laught.wav')
#boss muving class
    def update(self, player):
        if player.rect.x < self.rect.x:
            self.rect.move_ip((-self.speed, 0))
        else:
            self.rect.move_ip((self.speed, 0))
        self.process_shoting()

    def process_shoting(self):
        if self.current_shooting_cooldown <= 0:
            self.bullets.add(Bullet((self.rect.midbottom[0], self.rect.midbottom[1] + 20), 15))
            self.current_shooting_cooldown = self.shooting_cooldown
        else:
            self.current_shooting_cooldown -= 0.1 * self.clock.get_time()

        for bullet in list(self.bullets):
            if bullet.rect.bottom > HEIGHT:
                self.bullets.remove(bullet)
#boss generating class
    @staticmethod
    def process_bud_guy(clock, tik, bad_guys, bullets):
        if tik % 5 == 0:
            bad_guy = BadGuy(clock, bullets)
            bad_guy.bad_guy_sound.play()
            bad_guys.add(bad_guy)
#bullet class
class Bullet(pygame.sprite.Sprite):

    def __init__(self, position, speed):
        super(Bullet, self).__init__()

        if speed < 0:
            self.image = pygame.image.load('assets/player_bullet.png')
        else:
            self.image = pygame.image.load('assets/badguy_bullet.png')
        self.rect = self.image.get_rect()
        self.rect.midbottom = position
        self.speed = speed

    def update(self):
        self.rect.move_ip((0, self.speed))

    def get_speed(self):
        return self.speed

#health class
class Health(pygame.sprite.Sprite):

    def __init__(self):
        self.images = [pygame.image.load('assets/health_1.png'),
                       pygame.image.load('assets/health_2.png'),
                       pygame.image.load('assets/health_3.png')]

    def update(self, player, screen):
        if player.health == 2:
            screen.blit(self.images[2], (270, 10))
        elif player.health == 1:
            screen.blit(self.images[1], (270, 10))
        elif player.health == 0:
            screen.blit(self.images[0], (270, 10))

#player score class
class Score(object):

    def __init__(self):
        self.score = 1
        self.file = 'assets/score.txt'

    def update(self, screen):
        large_font = pygame.font.SysFont('comicsans', 30)
        text = large_font.render('Score: ' + str(self.score), 1, (255, 255, 255))
        screen.blit(text, (480, 10))

    def write_to_file(self):
        f = open(self.file, 'a')
        f.write(str(self.score) + '\n')
        f.close()