import pygame
import sys
import pyganim
from game_settings import SIZE, restart
from game_objects import Background, Player, Meteor, BadGuy, Health, Score
from menu import *
from mbuttons import *

pygame.init()
pygame.display.set_caption("Star ship|^|")

window = pygame.Surface(SIZE)

screen = pygame.display.set_mode(SIZE)
clock = pygame.time.Clock()


def startGame():
    game.menu(screen, window)
    explostion_animation = pyganim.PygAnimation([
        ('assets/blue_explosion/1_{}.png'.format(i), 50) for i in range(17)
    ], loop=False)
    explosions = []
    #add sounds to game
    pygame.mixer.music.load('assets/music/ES_Impulse.mp3')
    pygame.mixer.music.play(loops = -1)
    damage_sound = pygame.mixer.Sound('assets/music/damage.flac')
    #create sprites groups
    bullets = pygame.sprite.Group()
    all_objects = pygame.sprite.Group()
    meteors = pygame.sprite.Group()
    bad_guys = pygame.sprite.Group()

    background = Background()
    health = Health()
    score = Score()
    player = Player(clock, bullets)

    all_objects.add(background)
    all_objects.add(player)

    '''game.menu(screen, window)'''
    is_bad_guy_created = False
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
        Meteor.process_meteors(clock, meteors, pygame.time.get_ticks() * 1000)
        if is_bad_guy_created == False and score.score % 5 == 0:
            BadGuy.process_bud_guy(clock, (pygame.time.get_ticks() // 1000), bad_guys, bullets)
            is_bad_guy_created = True
        #objects positions updates
        all_objects.update()
        bullets.update()
        meteors.update()
        bad_guys.update(player)
        restartG = False
        ''''''
        keys = pygame.key.get_pressed()
        if keys[pygame.K_ESCAPE]:
                restartG = gameEsc.menu(screen, window)
                pygame.key.set_repeat(1, 1)
                pygame.mouse.set_visible(False)
        
        if keys[pygame.K_q] or restartG:
            return True
        
        #create meteors and bullets collisions list
        meteors_and_bullets_collided = pygame.sprite.groupcollide(meteors, bullets, True, True)
        score.score += len(meteors_and_bullets_collided)
        for collided in meteors_and_bullets_collided:
            explosion = explostion_animation.getCopy()
            explosion.play()
            explosions.append((explosion, collided.rect.center))

        # create bullets and badGuy_collided collisions list
        bullets_and_badGuy_collided = pygame.sprite.groupcollide(bad_guys, bullets, True, True)
        score.score += len(bullets_and_badGuy_collided) * 10
        player_and_meteors_collided = pygame.sprite.spritecollide(player, meteors, False)
        player_and_bullets_collided = pygame.sprite.spritecollide(player, bullets, False)

        for collided in bullets_and_badGuy_collided:
            explosion = explostion_animation.getCopy()
            explosion.play()
            explosions.append((explosion, collided.rect.center))

        if (player_and_meteors_collided or player_and_bullets_collided) and player.health == 0:
            score.write_to_file()
            all_objects.remove(player)
            Meteor.speed = 10
            restartG = gameF.menu(screen, window)
            #
            pygame.mouse.set_visible(False)
        elif player_and_meteors_collided or player_and_bullets_collided:
            damage_sound.play()
            if len(player_and_meteors_collided) >= 1:
                meteors.remove(player_and_meteors_collided.pop())
            else:
                bullets.remove(player_and_bullets_collided.pop())
            player.health -= 1

        if len(bad_guys) == 0:
            is_bad_guy_created = False

        all_objects.draw(screen)
        bullets.draw(screen)
        meteors.draw(screen)
        bad_guys.draw(screen)
        score.update(screen)
        health.update(player, screen)
        for explosion, position in explosions.copy():
            if explosion.isFinished():
                explosions.remove((explosion, position))
            else:
                x, y = position
                explosion.blit(screen, (x - 128, y - 128))

        pygame.display.flip()
        clock.tick(120)
        
def gameRunning():
    if startGame():
        gameRunning()

def main1():
    gameRunning()
    return 0

main1()