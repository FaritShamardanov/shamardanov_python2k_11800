from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.utils import timezone

# Create your models here.


def create_person(instance, created, **kwargs):
    if created:
        Person.objects.create(user=instance)


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    relationships = models.ManyToManyField('self', through='Relationship', related_name='related_to', symmetrical=False)

    def __str__(self):
        return self.user.username

    def add_relationship(self, person, status):
        relationship, created = Relationship.objects.get_or_create(
            from_person=self,
            to_person=person,
            status=status)
        return relationship

    def remove_relationship(self, person, status):
        Relationship.objects.filter(
            from_person=self,
            to_person=person,
            status=status).delete()
        return

    def get_relationships(self, status):
        return self.relationships.filter(
            to_people__status=status,
            to_people__from_person=self)

    def get_related_to(self, status):
        return self.related_to.filter(
            from_people__status=status,
            from_people__to_person=self)

    def get_following(self):
        return self.get_relationships(RELATIONSHIP_FOLLOWING)

    def get_followers(self):
        return self.get_related_to(RELATIONSHIP_FOLLOWING)

    post_save.connect(create_person, sender=User)


class Post(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='post_users')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    def published(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


RELATIONSHIP_FOLLOWING = 1
RELATIONSHIP_BLOCKED = 2
RELATIONSHIP_STATUS = (
  (RELATIONSHIP_FOLLOWING, 'Following'),
  (RELATIONSHIP_BLOCKED, 'Blocked'),
  )


class Relationship(models.Model):

    from_person = models.ForeignKey(Person, related_name='from_people', on_delete=models.DO_NOTHING)
    to_person = models.ForeignKey(Person, related_name='to_people', on_delete=models.DO_NOTHING)
    status = models.IntegerField(choices=RELATIONSHIP_STATUS)

    def __str__(self):
        return self.from_person.user.username + " " + RELATIONSHIP_STATUS[self.status - 1][1] \
               + " " + self.to_person.user.username
