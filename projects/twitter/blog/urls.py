from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('registration/', views.registration, name='registration'),
    path('login/', views.enter, name='login'),
    path('subscriptions/', views.subscriptions, name='subscriptions'),
    path('profile/', views.profile, name='profile'),
    path('logout/', views.logout_view, name='logout'),
    path('me/', views.me, name='my_profile'),
    path('profile/<int:user_id>/', views.profile, name='show_profile'),
    path('unsubscribe/<int:user_id>/', views.unsubscribe, name='unsubscribe'),
    path('posts/<int:user_id>/', views.show_user_posts, name='show_user_posts'),
]