from django.contrib import admin
from .models import Post, Person, Relationship

# Register your models here.

admin.site.register(Person)
admin.site.register(Post)
admin.site.register(Relationship)