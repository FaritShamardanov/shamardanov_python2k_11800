from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import UserCreationForm, AuthenticationForm, PostForm, RelationCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import Person, Post


# Create your views here.


def index(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.person_id = request.user.id
            form.save()
            return HttpResponseRedirect('/')
    form = PostForm()
    return render(request, 'blog/index.html', {'title': 'All posts', 'is_authenticated': request.user.is_authenticated,
                                               'posts': Post.objects.all().order_by("-created_date"),
                                               'form': form,
                                               'request': request})


def registration(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'blog/index.html', {'title': 'All posts',
                                                       'is_authenticated': request.user.is_authenticated,
                                                       'user_id': request.user.id})
    form = UserCreationForm()
    return render(request, 'blog/registration.html', {'form': form, 'title': 'Registration',
                                                      'is_authenticated': request.user.is_authenticated,
                                                      'user_id': request.user.id})


def enter(request):
    if request.method == "POST":
        email = request.POST['email']
        password = request.POST['password']
        if User.objects.filter(email=email).exists():
            username = getattr(User.objects.get(email=email), 'username')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('my_profile')
            else:
                return render(request, 'blog/registration.html', {'title': 'Registration',
                                                                  'is_authenticated': request.user.is_authenticated,
                                                                  'user_id': request.user.id})
        else:
            return render(request, 'blog/registration.html', {'title': 'Registration',
                                                              'is_authenticated': request.user.is_authenticated,
                                                              'user_id': request.user.id})
    form = AuthenticationForm()
    return render(request, 'blog/login.html', {'form': form, 'title': 'Login',
                                               'is_authenticated': request.user.is_authenticated,
                                               'user_id': request.user.id})


def logout_view(request):
    logout(request)
    return render(request, 'blog/index.html', {'title': 'All posts',
                                               'is_authenticated': request.user.is_authenticated,
                                               'user_id': request.user.id})


def subscriptions(request):
    following = Person.get_following(Person.objects.get(user_id=request.user.id))
    return render(request, 'blog/subscriptions.html', {'title': 'Subscriptions',
                                                       'is_authenticated': request.user.is_authenticated,
                                                       'user_id': request.user.id,
                                                       'following': following})


def unsubscribe(request, user_id):
    if request.method == "POST":
        Person.remove_relationship(Person.objects.get(user_id=request.user.id), Person.objects.get(user_id=user_id), 1)
        return render(request, 'blog/subscriptions.html', {'title': 'Subscriptions',
                                                           'is_authenticated': request.user.is_authenticated,
                                                           'user_id': request.user.id})


def profile(request, user_id):
    if request.method == "POST":
        Person.add_relationship(Person.objects.get(user_id=request.user.id), Person.objects.get(user_id=user_id), 1)
        return render(request, 'blog/subscriptions.html', {'title': 'Subscriptions',
                                                           'is_authenticated': request.user.is_authenticated,
                                                           'user_id': request.user.id})
    form = RelationCreationForm()
    following = Person.get_related_to(Person.objects.get(user_id=user_id), 1)
    on_follow = following.filter(user_id=request.user.id).first()
    return render(request, 'blog/profile.html', {'title': 'Profile',
                                                 'is_authenticated': request.user.is_authenticated,
                                                 'username': getattr(User.objects.get(id=user_id), 'username'),
                                                 'email': getattr(User.objects.get(id=user_id), 'email'),
                                                 'user_id': getattr(User.objects.get(id=user_id), 'id'),
                                                 'my_id': request.user.id,
                                                 'form': form,
                                                 'following': following,
                                                 'on_follow': on_follow})


def me(request):
    return render(request, 'blog/profile.html', {'title': 'Profile',
                                                 'is_authenticated': request.user.is_authenticated,
                                                 'username': request.user.username,
                                                 'email': request.user.email,
                                                 'user_id': request.user.id})


def show_user_posts(request, user_id):
    user_posts = Post.objects.filter(person_id=user_id)
    return render(request, 'blog/posts.html', {'title': 'Posts',
                                               'username': getattr(User.objects.get(id=user_id), 'username'),
                                               'is_authenticated': request.user.is_authenticated,
                                               'posts': user_posts})
