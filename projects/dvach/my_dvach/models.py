from django.db import models
import datetime

# Create your models here.


class Post(models.Model):
    text = models.TextField()
    published_date = models.DateTimeField(auto_now=datetime.datetime.now())
    reply_to = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True, related_name="replies")

    def __str__(self):
        return self.text