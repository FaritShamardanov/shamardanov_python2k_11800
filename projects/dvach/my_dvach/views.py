from django.shortcuts import render, get_object_or_404
from my_dvach.forms import PostForm, CommentForms
from my_dvach.models import Post
from django.shortcuts import redirect
from django.http import HttpResponseRedirect

# Create your views here.


def index(request):
    posts = Post.objects.all()
    form = PostForm()
    return render(request, 'index.html', context={'posts': posts, 'form': form})


def add_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            Post.objects.create(text=form.cleaned_data['text'])
            return HttpResponseRedirect('/')


def reply(request, id):
    if request.method == 'POST':
        formv = PostForm(request.POST)
        if formv.is_valid():
            Post.objects.create(text=formv.cleaned_data['text'], reply_to_id=id)
            return redirect('show_thread', id=id)
    form = CommentForms(initial={'id': id})
    return render(request, 'reply.html', context={'form': form})


def thread_index(request):
    if request.method == 'POST':
        new_thread = PostForm(request.POST)
        if new_thread.is_valid():
            Post.objects.create(text=new_thread.cleaned_data['text'])
            posts = Post.objects.all()
            form = PostForm()
            return redirect('show_thread', id=Post.objects.latest('published_date').id)
    posts = Post.objects.filter(reply_to__isnull=True).order_by('published_date')
    form = PostForm()
    return render(request, 'all_threads.html', context={'posts': posts, 'form': form})


def show_thread(request, id):
    post = Post.objects.filter(id=id)
    posts = Post.objects.filter(reply_to_id=id)
    form = PostForm()
    return render(request, 'index.html', context={'posts': post.union(posts).order_by('published_date'), 'form': form})